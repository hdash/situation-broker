package br.com.ufes.lprm.scene.broker.services;

import java.util.List;
import java.util.Map;

import br.com.ufes.lprm.scene.broker.models.context.Context;
import br.com.ufes.lprm.scene.broker.models.context.ContextValue;
import br.com.ufes.lprm.scene.broker.models.context.Entity;
import br.com.ufes.lprm.scene.broker.models.context.IntrinsicContext;
import br.com.ufes.lprm.scene.broker.models.context.RelationalContext;
import br.com.ufes.lprm.scene.broker.models.context.utils.Value;

public interface ContextBrokerService {
    public Entity registerEntity(Entity entity);
    public Entity registerEntityAttribute(Entity entity, String key, Value value);
    public Context registerIntrinsicContext(IntrinsicContext context);
    public Context registerRelationalContext(RelationalContext context, Map<String, String[]> parts);
    public ContextValue registerContextValue(Context context, ContextValue value);
    public boolean updateRules(String... rules);
}
