package br.com.ufes.lprm.scene.broker.scene;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Enumeration;
import java.util.concurrent.CompletableFuture;

import javax.inject.Named;
import javax.inject.Singleton;

import org.drools.compiler.kproject.models.KieModuleModelImpl;
import org.drools.core.ClockType;
import org.drools.core.impl.InternalKnowledgeBase;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.event.rule.RuleRuntimeEventListener;
import org.kie.api.io.Resource;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.api.runtime.rule.FactHandle;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.builder.KnowledgeBuilderFactory;
import org.kie.internal.io.ResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.ufes.inf.lprm.scene.SceneApplication;
import javassist.ClassPool;
import javassist.LoaderClassPath;

@Named
@Singleton
public class SceneBroker {

    private SceneApplication application;
    private CompletableFuture engine;
    private ClassPool classPool;

    private List<RuleRuntimeEventListener> listeners = new ArrayList<>();
    private static Logger logger = LoggerFactory.getLogger(SceneBroker.class);

    public SceneBroker() {
        this.initialize();
    }

    /**
     * Initializes the scene broker.
     */
    private void initialize(String... drls) {
        logger.debug("Starting scene actor");
        this.classPool = new ClassPool();
        String sceneId = "scene-broker";
        KieSession kieSession = createKieSession(sceneId);
        this.application = createSceneApplication(sceneId, kieSession, drls);
        engine = CompletableFuture.runAsync(kieSession::fireUntilHalt)
                .exceptionally(
                        err -> {
                            logger.error(err.toString());
                            this.reload();
                            return null;
                        });
    }

    /**
     * Stop scene application.
     */
    private void dispose() {
        logger.info("Shutting down SCENE engine...");
        this.engine.cancel(true);
        this.engine = null;
        this.application.getKsession().dispose();
        this.application = null;
        this.classPool = null;
    }

    /**
     * Create a new KieSession
     * 
     * @param sceneId Scene ID
     * @return KieSession
     */
    private KieSession createKieSession(String sceneId) {
        logger.info("Starting Kie...");
        ClassLoader loader = getClass().getClassLoader();
        classPool.appendSystemPath();
        classPool.appendClassPath(new LoaderClassPath(loader));

        // Getting KieServices
        KieServices kServices = KieServices.Factory.get();
        // Instantiating a KieFileSystem and KieModuleModel
        KieFileSystem kFileSystem = kServices.newKieFileSystem();
        KieModuleModel kieModuleModel = kServices.newKieModuleModel();

        ReleaseId releaseId = kServices.newReleaseId("br.ufes.inf.lprm.scene", sceneId, "0.1.0");
        kFileSystem.generateAndWritePomXML(releaseId);

        KieBaseModel kieBaseModel = kieModuleModel.newKieBaseModel(sceneId);
        kieBaseModel.setEventProcessingMode(EventProcessingOption.STREAM);
        // kieBaseModel.setEqualsBehavior(EqualityBehaviorOption.EQUALITY);
        kieBaseModel.addInclude("sceneKieBase");

        logger.info("Starting Kie Session...");
        KieSessionModel kieSessionModel = kieBaseModel.newKieSessionModel(sceneId + ".kSession");
        kieSessionModel.setClockType(ClockTypeOption.get(ClockType.REALTIME_CLOCK.getId()))
                .setType(KieSessionModel.KieSessionType.STATEFUL);
        // kieSessionModel.setClockType(ClockTypeOption.get(ClockType.PSEUDO_CLOCK.getId()))
        // .setType(KieSessionModel.KieSessionType.STATEFUL);

        kFileSystem.writeKModuleXML(kieModuleModel.toXML());
        KieBuilder kbuilder = kServices.newKieBuilder(kFileSystem);
        ArrayList<Resource> dependencies = new ArrayList<Resource>();
        try {
            Enumeration<URL> e = loader.getResources(KieModuleModelImpl.KMODULE_JAR_PATH);
            while (e.hasMoreElements()) {
                URL url = e.nextElement();
                String path;
                if (url.getPath().contains(".jar!")) {
                    path = url.getPath().replace("!/" + KieModuleModelImpl.KMODULE_JAR_PATH, "");
                    dependencies.add(kServices.getResources().newUrlResource(path));
                } else {
                    path = url.getPath().replace(KieModuleModelImpl.KMODULE_JAR_PATH, "");
                    dependencies.add(kServices.getResources().newFileSystemResource(path));
                }

            }
        } catch (IOException e1) {
            logger.error(e1.getMessage());
        }

        kbuilder.setDependencies(dependencies.toArray(new Resource[0]));
        kbuilder.buildAll();
        if (kbuilder.getResults().hasMessages()) {
            logger.error("Couldn't build knowledge module " + kbuilder.getResults());
            throw new IllegalArgumentException("Couldn't build knowledge module " + kbuilder.getResults());
        }

        logger.info("Starting Kie Module...");
        KieModule kModule = kbuilder.getKieModule();
        KieContainer kContainer = kServices.newKieContainer(kModule.getReleaseId());

        KieSession session = kContainer.newKieSession(sceneId + ".kSession");
        // session.setGlobal("logger", logger);
        // session.getKieRuntime(RuleRuntime.class).setGlobal("RANDOM_NUMBER",
        // Integer.parseInt(result.trim()));
        for (RuleRuntimeEventListener listener : listeners) {
            session.addEventListener(listener);
        }

        return session;
    }

    /**
     * Create a new Scene Application
     * 
     * @param sceneId the id of the scene application
     * @return the created Scene Application
     */
    private SceneApplication createSceneApplication(String sceneId, KieSession kieSession, String... drls) {
        if (drls != null) {
            this.addRules(kieSession, drls);
        }
        SceneApplication sceneApplication = new SceneApplication(classPool, kieSession, sceneId);

        return sceneApplication;
    }

    /**
     * Add rules to the Scene
     * 
     * @param drls the rules to be added
     */
    private void addRules(final KieSession kieSession, final String... drls) {
        for (final String drl : drls) {
            final KnowledgeBuilder kBuilder;
            kBuilder = createKnowledgeBuilder(kieSession.getKieBase(), drl);
            logger.info("Loading rule: ");
            ((InternalKnowledgeBase) kieSession.getKieBase()).addPackages(kBuilder.getKnowledgePackages());
        }
    }

    /**
     * Create a new KnowledgeBuilder with the given KieBase and DRLs.
     * 
     * @param kbase The KieBase to use.
     * @param drls  The DRLs to use.
     * @return A new KnowledgeBuilder.
     */
    private KnowledgeBuilder createKnowledgeBuilder(final KieBase kbase, final String... drls) {
        final KnowledgeBuilder kbuilder;
        if (kbase == null) {
            kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
        } else {
            kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder(kbase);
        }
        for (final String drl : drls) {
            kbuilder.add(ResourceFactory.newByteArrayResource(drl.getBytes()), ResourceType.DRL);
        }
        if (kbuilder.hasErrors()) {
            throw new RuntimeException("Knowledge contains errors: " + kbuilder.getErrors().toString());
        }
        return kbuilder;
    }

    /**
     * Insert object into the scene
     * 
     * @param object object to insert
     */
    public void insert(Object... objects) {
        logger.info("inserting object... {}", objects.length);
        KieSession kieSession = this.application.getKsession();
        kieSession.submit(session -> {
            for (Object object : objects) {
                if (session.getFactHandle(object) == null) {
                    logger.info("inserting object... {}", object);
                    session.insert(object);
                    logger.info("object inserted...");
                }
            }
        });

    }

    /**
     * Add a new listener to the scene
     * 
     * @param listener listener to be added
     */
    public void addEventListener(RuleRuntimeEventListener listener) {
        this.listeners.add(listener);
    }

    /**
     * Update object in the scene
     * 
     * @param object object to update
     */
    public void update(Object... objects) {
        KieSession kieSession = this.application.getKsession();
        kieSession.submit(session -> {
            for (Object object : objects) {
                FactHandle handle = session.getFactHandle(object);
                if (handle == null) {
                    logger.info("fact handle for `{}` not found... inserting it", object);
                    logger.info("inserting object... {}", object.toString());
                    session.insert(object);
                    logger.info("object inserted...");
                } else {
                    logger.info("updating object... {}", object.toString());
                    session.update(handle, object);
                    logger.info("object updated...");
                }
            }
        });
    }

    /**
     * Delete object from the scene
     * 
     * @param object object to delete
     */
    public void delete(Object object) {
        KieSession kieSession = this.application.getKsession();
        kieSession.submit(
                session -> session.delete(session.getFactHandle(object)));
    }

    /**
     * Reload scene application
     * 
     * @param drls
     */
    public void reload(String... drls) {
        if (this.application != null) {
            this.dispose();
        }
        this.initialize(drls);
    }
}
