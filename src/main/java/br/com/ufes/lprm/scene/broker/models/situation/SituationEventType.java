package br.com.ufes.lprm.scene.broker.models.situation;

public enum SituationEventType { ACTIVATION, DEACTIVATION }
