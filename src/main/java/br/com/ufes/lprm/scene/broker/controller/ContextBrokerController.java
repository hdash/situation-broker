package br.com.ufes.lprm.scene.broker.controller;

import static org.springdoc.core.Constants.SWAGGER_UI_PATH;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.ufes.lprm.scene.broker.models.context.Context;
import br.com.ufes.lprm.scene.broker.models.context.ContextValue;
import br.com.ufes.lprm.scene.broker.models.context.Entity;
import br.com.ufes.lprm.scene.broker.models.context.IntrinsicContext;
import br.com.ufes.lprm.scene.broker.models.context.RelationalContext;
import br.com.ufes.lprm.scene.broker.repositories.ContextRepository;
import br.com.ufes.lprm.scene.broker.repositories.EntityRepository;
import br.com.ufes.lprm.scene.broker.services.ContextBrokerService;

@RestController
public class ContextBrokerController {

    private final EntityRepository entityRepo;
    private final ContextRepository contextRepo;
    private final ContextBrokerService contextBrokerService;

    @Value(SWAGGER_UI_PATH)
    private String swaggerUiPath;

    @Inject
    public ContextBrokerController(
            EntityRepository entityRepo,
            ContextRepository contextRepo,
            ContextBrokerService contextBrokerService) {
        this.entityRepo = entityRepo;
        this.contextRepo = contextRepo;
        this.contextBrokerService = contextBrokerService;
    }

    @RequestMapping("/")
    public void apiDocumentation(HttpServletResponse response) throws IOException {
        response.sendRedirect(swaggerUiPath);
    }

    @GetMapping("/relations/{relationId}")
    public Context getRelationById(@PathVariable Long relationId) {
        Optional<Context> context = contextRepo.getById(relationId);
        return context.get();
    }

    @PostMapping("/relations/{relationId}/values")
    public ContextValue registerRelationValue(@RequestBody ContextValue value, @PathVariable Long relationId) {
        Optional<Context> context = contextRepo.getById(relationId);
        contextBrokerService.registerContextValue(context.get(), value);
        return value;
    }

    @PostMapping("/entities")
    public Entity registerEntity(@RequestBody Entity entity) {
        entity = contextBrokerService.registerEntity(entity);
        return entity;
    }

    @GetMapping("/entities")
    public Collection<Entity> getEntities() {
        return entityRepo.getAll();
    }

    @PostMapping("/entities/{entityId}/contexts")
    public IntrinsicContext registerIntrinsicContext(@RequestBody IntrinsicContext context,
            @PathVariable Long entityId) {
        Optional<Entity> bearerOpt = entityRepo.getById(entityId);
        context.setBearer(bearerOpt.get());
        bearerOpt.get().getContexts().put(context.getKind(), context);
        contextBrokerService.registerIntrinsicContext(context);
        return context;
    }

    @PostMapping("/entities/{entityId}/contexts/{contextId}/values")
    public ContextValue registerContextValue(@RequestBody ContextValue value, @PathVariable long entityId,
            @PathVariable long contextId) {
        Optional<Context> context = contextRepo.getById(contextId);
        contextBrokerService.registerContextValue(context.get(), value);
        return value;
    }

    @PostMapping("/entities/{entityId}/contexts/{contextId}/values/bulk")
    public List<ContextValue> registerBulkContextValue(@RequestBody List<ContextValue> values,
            @PathVariable long entityId,
            @PathVariable long contextId) {
        Context context = contextRepo.getById(contextId).get();
        for (ContextValue value : values) {
            contextBrokerService.registerContextValue(context, value);
            try {
                // TODO: wrong way. It should be fixed in the future.
                Thread.sleep(2L);
            } catch (InterruptedException e) {
            }
        }
        return values;
    }

    /*
     * public Result getContext(Request request, Long entityId) {
     * return TODO(request);
     * }
     * 
     * public Result getContextValue(Request request) {
     * return TODO(request);
     * }
     * 
     * public Result registerAttribute(Request request, Long entityId) {
     * return TODO(request);
     * }
     * 
     * public Result getAttributes(Request request, Long entityId) {
     * return TODO(request);
     * }
     */

    @PostMapping("/relations")
    public RelationalContext createRelationalContext(@RequestBody RelationalContext relation) {

        /*
         * JsonNode json = request.body().asJson();
         * RelationalContext newOne = Json.fromJson(json, RelationalContext.class);
         * contextBrokerService.registerRelationalContext(relation,
         * request.queryString());
         * return ok(Json.toJson(newOne));
         */
        return null;
    }
}
