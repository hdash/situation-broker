package br.com.ufes.lprm.scene.broker.io.json.mixins;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import br.com.ufes.lprm.scene.broker.io.json.deserializers.ValueDeserializer;
import br.com.ufes.lprm.scene.broker.io.json.serializers.ValueSerializer;

@JsonDeserialize(using = ValueDeserializer.class)
@JsonSerialize(using = ValueSerializer.class)
public abstract class ValueMixin { }
