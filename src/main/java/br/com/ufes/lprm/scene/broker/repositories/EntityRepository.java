package br.com.ufes.lprm.scene.broker.repositories;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.inject.Named;
import javax.inject.Singleton;

import br.com.ufes.lprm.scene.broker.models.context.Entity;

@Named
@Singleton
public class EntityRepository {

    private Map<Long, Entity> entities = new HashMap<>();

    public Entity save(Entity entity) {
        entity.setId(entities.size() + 1);
        entities.put(entity.getId(), entity);
        return entity;
    }

    public Collection<Entity> getAll() {
        return entities.values();
    }

    public Optional<Entity> getById(Long id) {
        return Optional.ofNullable(entities.get(id));
    }

}
