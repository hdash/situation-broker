package br.com.ufes.lprm.scene.broker.services;

import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import br.com.ufes.lprm.scene.broker.models.context.Context;
import br.com.ufes.lprm.scene.broker.models.context.ContextValue;
import br.com.ufes.lprm.scene.broker.models.context.Entity;
import br.com.ufes.lprm.scene.broker.models.context.IntrinsicContext;
import br.com.ufes.lprm.scene.broker.models.context.RelationalContext;
import br.com.ufes.lprm.scene.broker.models.context.utils.Value;
import br.com.ufes.lprm.scene.broker.repositories.ContextRepository;
import br.com.ufes.lprm.scene.broker.repositories.EntityRepository;
import br.com.ufes.lprm.scene.broker.scene.SceneBroker;

@Singleton
@Named
public class SceneBrokerService implements ContextBrokerService {

    private final EntityRepository entityRepo;
    private final ContextRepository contextRepo;
    private final SceneBroker sceneBroker;

    @Inject
    SceneBrokerService(EntityRepository entityRepo, ContextRepository contextRepo, SceneBroker sceneBroker) {
        this.entityRepo = entityRepo;
        this.contextRepo = contextRepo;
        this.sceneBroker = sceneBroker;
    }

    @Override
    public Entity registerEntity(Entity entity) {
        entityRepo.save(entity);
        sceneBroker.insert(entity);
        return entity;
    }

    @Override
    public Entity registerEntityAttribute(Entity entity, String key, Value value) {
        return null;
    }

    @Override
    public Context registerIntrinsicContext(IntrinsicContext context) {
        contextRepo.save(context);
        sceneBroker.insert(context);
        return context;
    }

    @Override
    public Context registerRelationalContext(RelationalContext context, Map<String, String[]> parts) {
        parts.forEach((key, value) -> {
            Optional<Entity> ent = entityRepo.getById(Long.valueOf(value[0]));
            if (ent.isPresent()) {
                context.getParts().put(key, ent.get());
            } else
                throw new RuntimeException(String.format("entity not found for %s and id %s", key, value[0]));
        });
        contextRepo.save(context);
        return context;
    }

    @Override
    public ContextValue registerContextValue(Context context, ContextValue value) {
        value.setOwner(context);
        context.setValue(value);
        sceneBroker.insert(value);
        sceneBroker.update(value.getOwner());
        return value;
    }

    @Override
    public boolean updateRules(String... rules) {
        this.sceneBroker.reload(rules);
        return true;
    }

}
