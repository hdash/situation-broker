package br.com.ufes.lprm.scene.broker.io.json.mixins;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import br.com.ufes.lprm.scene.broker.models.context.Entity;

public abstract class IntrinsicContextMixin {

    @JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    private Entity bearer;
}
