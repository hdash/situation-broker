package br.com.ufes.lprm.scene.broker.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.ufes.lprm.scene.broker.models.situation.PersistentSituation;
import br.com.ufes.lprm.scene.broker.repositories.SituationRepository;
import br.com.ufes.lprm.scene.broker.services.ContextBrokerService;

@RestController
public class SituationBrokerController {

    private final ContextBrokerService contextBrokerService;
    private final SituationRepository situationRepository;

    @Inject
    public SituationBrokerController(
            SituationRepository situationRepository,
            ContextBrokerService contextBrokerService) {
        this.contextBrokerService = contextBrokerService;
        this.situationRepository = situationRepository;
    }

    @PostMapping("/situations/upload")
    public ResponseEntity<Map<String, Object>> uploadDRL(@RequestBody Map<String, List<String>> value) {
        Map<String, Object> response = new HashMap<String, Object>() {
            {
                put("success", true);
            }
        };
        try {
            List<String> situations = null;
            if (value != null) {
                situations = value.get("situations");
                contextBrokerService.updateRules(situations.toArray(new String[situations.size()]));
            }
        } catch (Exception e) {
            response.put("success", false);
            response.put("error", e.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/situations/{situationId}")
    public PersistentSituation getSituation(@PathVariable Long situationId) {
        return situationRepository
                .getById(situationId).get();
    }

    @GetMapping("/situations")
    public List<PersistentSituation> getSituations() {
        return situationRepository.getAll();
    }
}
