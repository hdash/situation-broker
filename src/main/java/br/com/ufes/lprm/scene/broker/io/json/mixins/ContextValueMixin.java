package br.com.ufes.lprm.scene.broker.io.json.mixins;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.ufes.lprm.scene.broker.models.context.Context;

public class ContextValueMixin {

    //@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
    //@JsonIdentityReference(alwaysAsId=true)
    @JsonIgnore
    private Context owner;
}
