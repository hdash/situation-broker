package br.com.ufes.lprm.scene.broker.models.context.utils;

public enum ValueType { NUMBER, STRING, BOOLEAN }
