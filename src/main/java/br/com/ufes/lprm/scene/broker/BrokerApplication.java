package br.com.ufes.lprm.scene.broker;

import com.fasterxml.jackson.annotation.JsonInclude;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import br.com.ufes.lprm.scene.broker.io.json.mixins.ContextMixin;
import br.com.ufes.lprm.scene.broker.io.json.mixins.ContextValueMixin;
import br.com.ufes.lprm.scene.broker.io.json.mixins.EntityMixin;
import br.com.ufes.lprm.scene.broker.io.json.mixins.IntrinsicContextMixin;
import br.com.ufes.lprm.scene.broker.io.json.mixins.RelationalContextMixin;
import br.com.ufes.lprm.scene.broker.io.json.mixins.SituationMixin;
import br.com.ufes.lprm.scene.broker.io.json.mixins.ValueMixin;
import br.com.ufes.lprm.scene.broker.models.context.Context;
import br.com.ufes.lprm.scene.broker.models.context.ContextValue;
import br.com.ufes.lprm.scene.broker.models.context.Entity;
import br.com.ufes.lprm.scene.broker.models.context.IntrinsicContext;
import br.com.ufes.lprm.scene.broker.models.context.RelationalContext;
import br.com.ufes.lprm.scene.broker.models.context.utils.Value;
import br.com.ufes.lprm.scene.broker.models.situation.PersistentSituation;
import br.com.ufes.lprm.scene.broker.spring.SpringConfigurator;

@SpringBootApplication
public class BrokerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrokerApplication.class, args);
	}

	@Bean
	public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
		Jackson2ObjectMapperBuilder builder = new Jackson2ObjectMapperBuilder()
				.serializationInclusion(JsonInclude.Include.NON_NULL);
		builder.mixIn(Entity.class, EntityMixin.class);
		builder.mixIn(Value.class, ValueMixin.class);
		builder.mixIn(Context.class, ContextMixin.class);
		builder.mixIn(IntrinsicContext.class, IntrinsicContextMixin.class);
		builder.mixIn(RelationalContext.class, RelationalContextMixin.class);
		builder.mixIn(ContextValue.class, ContextValueMixin.class);
		builder.mixIn(PersistentSituation.class, SituationMixin.class);
		return new MappingJackson2HttpMessageConverter(builder.build());
	}

	@Bean
	public SpringConfigurator springConfigurator() {
		return new SpringConfigurator(); // This is just to get context
	}
}
