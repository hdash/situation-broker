package br.com.ufes.lprm.scene.broker.io.json.mixins;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import br.com.ufes.lprm.scene.broker.io.json.deserializers.SituationDeserializer;
import br.com.ufes.lprm.scene.broker.io.json.serializers.PersistentSituationSerializer;

@JsonSerialize(using = PersistentSituationSerializer.class)
@JsonDeserialize(using = SituationDeserializer.class)
public class SituationMixin {
}
