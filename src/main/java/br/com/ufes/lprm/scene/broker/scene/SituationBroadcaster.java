package br.com.ufes.lprm.scene.broker.scene;

import java.io.IOException;
import java.util.Optional;
import java.util.Set;

import javax.websocket.Session;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.kie.api.event.rule.DefaultRuleRuntimeEventListener;
import org.kie.api.event.rule.ObjectInsertedEvent;
import org.kie.api.event.rule.ObjectUpdatedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import br.com.ufes.lprm.scene.broker.models.situation.PersistentSituation;
import br.com.ufes.lprm.scene.broker.models.situation.SituationEvent;
import br.com.ufes.lprm.scene.broker.models.situation.SituationEventType;
import br.com.ufes.lprm.scene.broker.repositories.SituationRepository;
import br.ufes.inf.lprm.situation.model.Situation;

public class SituationBroadcaster extends DefaultRuleRuntimeEventListener {

    private final Set<Session> subscribers;
    private final SituationRepository situationRepo;
    private final ObjectMapper mapper;
    private static Logger logger = LoggerFactory.getLogger(SituationBroadcaster.class);

    public SituationBroadcaster(Set<Session> subscribers, SituationRepository situationRepo, ObjectMapper mapper) {
        this.subscribers = subscribers;
        this.situationRepo = situationRepo;
        this.mapper = mapper;
    }

    public String beautify(JsonNode jsonNode) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Object json = mapper.readValue(jsonNode.toString(), Object.class);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
    }

    @Override
    public void objectInserted(ObjectInsertedEvent event) {
        try {
            Object in = event.getObject();
            if (in instanceof Situation) {
                PersistentSituation situation = situationRepo.save((Situation) in);
                SituationEvent activation = new SituationEvent(SituationEventType.ACTIVATION,
                        ((Situation) in).getActivation().getTimestamp(), situation);

                logger.info(mapper.writeValueAsString(activation));
                for (Session subscriber : subscribers) {
                    subscriber.getBasicRemote().sendText(mapper.writeValueAsString(activation));
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    public void objectUpdated(ObjectUpdatedEvent event) {
        try {
            Object in = event.getObject();
            if (in instanceof Situation) {
                Optional<PersistentSituation> situationOpt = situationRepo.getByOngoingId(((Situation) in).getUID());
                situationRepo.removeOngoing(((Situation) in).getUID());
                if (!situationOpt.isPresent())
                    throw new RuntimeException("situation not found");
                SituationEvent deactivation = new SituationEvent(SituationEventType.DEACTIVATION,
                        ((Situation) in).getDeactivation().getTimestamp(), situationOpt.get());

                logger.info(mapper.writeValueAsString(deactivation));
                for (Session subscriber : subscribers) {
                    subscriber.getBasicRemote().sendText(mapper.writeValueAsString(deactivation));
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

}
