package br.com.ufes.lprm.scene.broker.websocket;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import br.com.ufes.lprm.scene.broker.repositories.SituationRepository;
import br.com.ufes.lprm.scene.broker.scene.SceneBroker;
import br.com.ufes.lprm.scene.broker.scene.SituationBroadcaster;
import br.com.ufes.lprm.scene.broker.spring.SpringConfigurator;

@ServerEndpoint(value = "/ws", configurator = SpringConfigurator.class)
@Named
public class WebsocketSituationEndpoint {

    private final ObjectMapper mapper;
    private final Set<Session> subscribers = new HashSet<>();
    private static Logger logger = LoggerFactory.getLogger(SituationBroadcaster.class);

    @Inject
    public WebsocketSituationEndpoint(MappingJackson2HttpMessageConverter converter, SceneBroker broker,
            SituationRepository situationRepo) {
        this.mapper = converter.getObjectMapper();
        broker.addEventListener(new SituationBroadcaster(subscribers, situationRepo, mapper));
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        logger.info("subscribers size: `{}`", subscribers.size());
        logger.info("adding a new subscriber for `{}`", session.getRequestURI().toString());
        subscribers.add(session);
        ObjectNode node = this.mapper.createObjectNode();
        node.put("heartbeat", true);
        session.getBasicRemote().sendText(node.toString());
    }

    @OnMessage
    public void onMessage(Session session, String message) throws IOException {
        for (Session subscriber : subscribers) {
            subscriber.getBasicRemote().sendText(message);
        }
    }

    @OnClose
    public void onClose(Session session) throws IOException {
        logger.info("removing subscriber for `{}`", session.getRequestURI().toString());
        subscribers.remove(session);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {
    }
}