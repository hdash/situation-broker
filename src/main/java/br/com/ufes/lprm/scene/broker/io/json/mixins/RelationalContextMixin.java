package br.com.ufes.lprm.scene.broker.io.json.mixins;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import br.com.ufes.lprm.scene.broker.io.json.serializers.RelationalContextSerializer;

@JsonSerialize(using = RelationalContextSerializer.class)
public abstract class RelationalContextMixin {

}
